# README #

This is a super message queue implementation

### To-do features ###

From most important to less important:

* Fault/Outage tolerant
* For single consumer per queue, message order is preserved
* For multiple consumer per queue, order is not.
* Garantee delivery a message at least once (Like SQS)
* Maximize the throughout for all queus, and concurrency per queue
* Message operations (prioritized deliver, scheduled deliver)

### Features hard-to-do ###

* Across region syncing and thus distributed queue


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact